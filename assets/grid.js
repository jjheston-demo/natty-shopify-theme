//-------------------------------------------------------------------------------------------------------------------
// In .row.of-three, depending on @media queries, all items in a "row" need to be dynamically assign the same height
//-------------------------------------------------------------------------------------------------------------------
( function(){

  var max_height = 0;
  var set = [];

  setInterval( equalize_heights, 400 );

  function equalize_heights(){
    var $spans = $( '.row.of-three .span' );
    var width = $( window ).width();
    var n = 0;
    // small
    if( width <= 500 ){
      n = 1;
    }
    // medium
    else if( width > 500 && width <= 768  ){
      n = 2;
    }
    // large
    else if( width > 768 && width <= 939 ){
      n = 3;
    }
    // full
    else{
      n = 3;
    }

    // loop through set of n first, finding max height
    $.each( $spans, function( index, span ){ 
      var $span = $( span );
      // reset
      if( index % n === 0 ){
        process_set();
        max_height = 0;
      }
      if( index % n !== 0 || max_height === 0 ){
        set.push( $span );
      }

      var this_height = $span.children( '*' ).height();
      if( this_height > max_height ){
        max_height = this_height;
      }
    } );
  } 
  
  function process_set(){
    $.each( set, function( index, span ){ 
      var $span = $( span );
      $span.height( max_height );
    } );
    set.length = 0;

  }

} )();