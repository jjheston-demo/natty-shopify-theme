var Global = ( function( $ ){
  var
  pub = {},
  priv = {},
  bknd;

  pub.init = function(){
  	activate_mobile_nav();
    activate_footer_quotes_pagination();
  }

  function activate_footer_quotes_pagination(){
    $('ul.bxslider#footer-quotes-pager').bxSlider( {
      pager: false,
      adaptiveHeight: true
      //mode: 'fade'
    } );

  }

  //-----------------------------------
  // Open/Close Mobile Navigation Menu
  //-----------------------------------
  function activate_mobile_nav(){
  	var
  	$button = $( 'header .toggle-nav' ),
  	$mobile_nav = $( '#page-template .mobile-nav' ),
  	bknd;

    $button.click( function( event ){
      event.stopPropagation();
      $mobile_nav.toggleClass( 'active' );
      active = true;
    } );

    $( 'html' ).click( function(){
      $mobile_nav.removeClass( 'active' );
    } );

    $mobile_nav.click( function( event ){
      event.stopPropagation();
    } );
  }

  return pub;

}( jQuery ) );// module definition

$( document ).ready( function(){
    
  Global.init();

} );// doc ready